import slippersTheme from './SlippersTheme';

import { addons } from '@storybook/addons';

addons.setConfig({
  panelPosition: 'right',
  theme: slippersTheme
});
