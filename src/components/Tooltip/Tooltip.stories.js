import SlpTooltip from "./Tooltip.vue";

export default {
  title: "Components/Tooltip",
  component: SlpTooltip,
  argTypes: {
    position: {
      control: { type: "select" },
      options: ["right", "left", "top", "bottom"],
    },
  },
};

const Template = (args, { argTypes }) => {
  return {
    components: { SlpTooltip },
    props: Object.keys(argTypes),
    template: `<SlpTooltip v-bind="$props">What is Lorem Ipsum?
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </SlpTooltip>`,
  };
};

export const Default = Template.bind({});

Default.args = {
  position: "bottom",
};
