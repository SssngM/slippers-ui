import SlpLink from "./Link.vue";

export default {
  title: "Components/Link",
  component: SlpLink,
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpLink },
  props: Object.keys(argTypes),
  template: '<SlpLink v-bind="$props">Link</SlpLink>',
});

export const Default = Template.bind({});
Default.args = {
  arrow: false,
  href: "https://about.gitlab.com",
};
