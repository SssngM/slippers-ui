import SlpRadio from "./Radio";

export default {
  title: "Components/Radio",
  component: SlpRadio,
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpRadio },
  props: Object.keys(argTypes),
  template: '<SlpRadio v-bind="$props">Radio</SlpRadio>',
});

export const Default = Template.bind({});
Default.args = {
  option: "Option",
};
